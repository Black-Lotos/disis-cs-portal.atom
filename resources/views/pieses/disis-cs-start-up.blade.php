@extends('common.site-master-page')
@section('output-info')
    @isset($page['sub-page-message'])
        @if(!is_null($page['sub-page-message']))
            <h1 class="text-center bg-info text-white" id="inMessage">{{$page['sub-page-message']}}</h1>
        @endif
    @endisset
@stop
@section('content')
    {{$page['content']['middle']}}
@stop
