@extends('common.site-master-page')
@section('output-info')
    @isset($page['sub-page-message'])
        @if(!is_null($page['sub-page-message']))
            <h1 class="text-center bg-info text-white" id="inMessage">{{$page['sub-page-message']}}</h1>
        @endif
    @endisset
@stop
@section('content')
    {{$page['content']['middle']}}
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">№</th>
            <th scope="col">Назва товару</th>
            <th scope="col">Модель товару</th>
            <th scope="col">Арт.</th>
            <th scope="col">Од. вим.</th>
            <th scope="col">Кіл-ть в упак.</th>
            <th scope="col">Вартість</th>
            <th scope="col">Група товару</th>
            <th scope="col">Бренд товару</th>
            <th scope="col">Дія</th>
        </tr>
        </thead>
        <tbody>
        <?php //dd((!empty($data) and count($data)>0)); ?>
        @if (!empty($data) and count($data)>0)
            @foreach($data as $inKey => $inItem)
                @if (!empty($inItem))
                    <tr>
                        <th scope="row" width="30px">{{$inItem['UnitID']}}</th>
                        <td>{{$inItem['UnitName']}}</td>
                        <td>{{$inItem['UnitModel']}}</td>
                        <td>{{$inItem['UnitArt']}}</td>
                        <td>{{$inItem['UnitProp']}}</td>
                        <td>{{$inItem['UnitCountToBox']}}</td>
                        <td>{{$inItem['UnitCost']}}</td>
                        <td>{{$inItem['GroupName']}}</td>
                        <td>{{$inItem['BrandName']}}</td>
                        <td>
                            <a id='ref-tovar-edit-{{$inItem['UnitID']}}' class='action-base action-edit' title='Коригувати' href="/Tovar/edit/{{$inItem['UnitID']}}"></a>
                            <a id='ref-tovar-delete-{{$inItem['UnitID']}}' class='action-base action-delete' title='Видалити' href="/Tovar/delete/{{$inItem['UnitID']}}"></a>
                        </td>
                    </tr>
                @endif
            @endforeach
            <tr>
                <td colspan="10" class="bg-transparent">
                    <a href="/Tovar/add" class="btn btn-info" role="button">Додати товар</a>
                    <a href="/Tovar/export" class="btn btn-success" role="button">Экспорт товару</a>
                </td>
            </tr>
        @else
            <h2>
                Таблиця товарів порожня. Необхідно додати дані.
                <a href="/Tovar/add" class="btn btn-info" role="button">Додати товар</a>
            </h2>
        @endif
        </tbody>
    </table>
    <div class="col-md-12 text-center">

    </div>
@stop
