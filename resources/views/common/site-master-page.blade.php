<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$page['property']['tittle']}}</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/css/style-overload.css" rel="stylesheet" type="text/css">

</head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
          <a class="navbar-brand" href="#">{{$page['property']['tittle']}}</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                @foreach ($page['menu'] as $inKey => $inItem)
                    @if (!empty($inItem['sub_menu']) and is_array($inItem['sub_menu']))
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="" id="{{$inKey}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$inItem['caption']}}</a>
                            <div class="dropdown-menu" aria-labelledby="{{$inKey}}">
                                @foreach ($inItem['sub_menu'] as $inSubKey => $inSubItem)
                                    <a class="dropdown-item" href="{{$inSubItem['set_url']}}">{{$inSubItem['caption']}}</a>
                                @endforeach
                            </div>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{$inItem['mi_url']}}">{{$inItem['tittle']}}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
            @isset($page['menu-language'])
            <ul class="nav navbar-nav navbar-right">
                @foreach ($page['menu-language'] as $inKey => $inItem)
                    <li class="nav-item">
                        <a class="nav-link" href="{{$inItem['mi_url']}}"><img src="{{$inItem['mi_img']}}" width="30px" height="20x"> {{$inItem['tittle']}}</a>
                    </li>
                @endforeach
            </ul>
            @endisset
          </div>
        </nav>
        <main role="main" class="container">
          <div class="">
            @yield('output-info')
            @yield('send-email')
            @yield('content')
          </div>
        </main>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="/js/common.js"></script>
    </body>
</html>
