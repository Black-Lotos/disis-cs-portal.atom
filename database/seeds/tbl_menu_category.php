<?php

use Illuminate\Database\Seeder;

class tbl_menu_category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $tablename='tbl_menu_category';

    public function run()
    {
        /*DB::table($this->tablename)->insert([
            'menu_category_code' => 'menu-default',
            'tittle' => 'Меню по умолчанию',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_category_code' => 'menu-admin',
            'tittle' => 'Верхнее меню администратора',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_category_code' => 'menu-admin-left',
            'tittle' => 'Левое меню администратора',
            'created_at' => now(),
        ]);*/

        DB::table($this->tablename)->insert([
            'menu_category_code' => 'menu-language',
            'tittle' => 'Меню выбора языка',
            'created_at' => now(),
        ]);
    }
}
