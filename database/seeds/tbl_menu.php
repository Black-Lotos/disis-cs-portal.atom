<?php

use Illuminate\Database\Seeder;

class tbl_menu extends Seeder
{
    protected $tablename='tbl_menu';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table($this->tablename)->insert([
            'menu_category_id' => 1,
            'menu_category_code' => 'menu-default',
            'menu_code' =>'mi_000001',
            'mi_url' =>'https://laravel.com/docs',
            'tittle' => 'Docs',
            'mi_owner' => 0,
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_category_id' => 1,
            'menu_category_code' => 'menu-default',
            'menu_code' =>'mi_000002',
            'mi_url' =>'https://laracasts.com',
            'tittle' => 'Laracasts',
            'mi_owner' => 0,
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_category_id' => 1,
            'menu_category_code' => 'menu-default',
            'menu_code' =>'mi_000003',
            'mi_url' =>'https://laravel-news.com',
            'tittle' => 'News',
            'mi_owner' => 0,
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_category_id' => 1,
            'menu_category_code' => 'menu-default',
            'menu_code' =>'mi_000004',
            'mi_url' =>'https://blog.laravel.com',
            'tittle' => 'Blog',
            'mi_owner' => 0,
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_category_id' => 1,
            'menu_category_code' => 'menu-default',
            'menu_code' =>'mi_000005',
            'mi_url' =>'https://nova.laravel.com',
            'tittle' => 'Nova',
            'mi_owner' => 0,
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_category_id' => 1,
            'menu_category_code' => 'menu-default',
            'menu_code' =>'mi_000006',
            'mi_url' =>'https://forge.laravel.com',
            'tittle' => 'Forge',
            'mi_owner' => 0,
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_category_id' => 1,
            'menu_category_code' => 'menu-default',
            'menu_code' =>'mi_000007',
            'mi_url' =>'https://vapor.laravel.com',
            'tittle' => 'Vapor',
            'mi_owner' => 0,
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_category_id' => 1,
            'menu_category_code' => 'menu-default',
            'menu_code' =>'mi_000008',
            'mi_url' =>'https://github.com/laravel/laravel',
            'tittle' => 'GitHub',
            'mi_owner' => 0,
            'created_at' => now(),
        ]);

        DB::table($this->tablename)->insert([
            'menu_category_id' => 4,
            'menu_category_code' => 'menu-language',
            'menu_code' =>'ml_000001',
            'mi_url' =>'/lang/ua',
            'mi_img' =>'/img/flag_ukraine.png',
            'tittle' => 'Український',
            'mi_owner' => 0,
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_category_id' => 4,
            'menu_category_code' => 'menu-language',
            'menu_code' =>'ml_000002',
            'mi_url' =>'/lang/ru',
            'mi_img' =>'/img/flag_russia.png',
            'tittle' => 'Русский',
            'mi_owner' => 0,
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_category_id' => 4,
            'menu_category_code' => 'menu-language',
            'menu_code' =>'ml_000003',
            'mi_url' =>'/lang/en',
            'mi_img' =>'/img/flag_great_britain.png',
            'tittle' => 'English',
            'mi_owner' => 0,
            'created_at' => now(),
        ]);

    }
}
