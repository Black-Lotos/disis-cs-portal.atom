<?php

use Illuminate\Database\Seeder;

class tbl_menu_to_language extends Seeder
{
    protected $tablename='tbl_menu_to_language';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table($this->tablename)->insert([
            'menu_id' => 1,
            'menu_code' =>'mi_000001',
            'languages_id' =>1,
            'languages_code' =>'ua',
            'tittle' => 'Документація',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 2,
            'menu_code' =>'mi_000002',
            'languages_id' => 1,
            'languages_code' => 'ua',
            'tittle' => 'Laracasts',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 3,
            'menu_code' =>'mi_000003',
            'languages_id' => 1,
            'languages_code' => 'ua',
            'tittle' => 'Новини',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 4,
            'menu_code' =>'mi_000004',
            'languages_id' => 1,
            'languages_code' => 'ua',
            'tittle' => 'Обговорення',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 5,
            'menu_code' =>'mi_000005',
            'languages_id' => 1,
            'languages_code' => 'ua',
            'tittle' => 'Адміністративна панель',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 6,
            'menu_code' =>'mi_000006',
            'languages_id' => 1,
            'languages_code' => 'ua',
            'tittle' => 'СМС сайту',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 7,
            'menu_code' =>'mi_000007',
            'languages_id' => 1,
            'languages_code' => 'ua',
            'tittle' => 'Vapor',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 8,
            'menu_code' =>'mi_000008',
            'languages_id' => 1,
            'languages_code' => 'ua',
            'tittle' => 'ГітХаб',
            'created_at' => now(),
        ]);
        //
        DB::table($this->tablename)->insert([
            'menu_id' => 1,
            'menu_code' =>'mi_000001',
            'languages_id' => 2,
            'languages_code' =>'ru',
            'tittle' => 'Документация',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 2,
            'menu_code' =>'mi_000002',
            'languages_id' => 2,
            'languages_code' => 'ru',
            'tittle' => 'Laracasts',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 3,
            'menu_code' =>'mi_000003',
            'languages_id' => 2,
            'languages_code' => 'ru',
            'tittle' => 'Новости',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 4,
            'menu_code' =>'mi_000004',
            'languages_id' => 2,
            'languages_code' => 'ru',
            'tittle' => 'Форум',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 5,
            'menu_code' =>'mi_000005',
            'languages_id' => 2,
            'languages_code' => 'ru',
            'tittle' => 'Административная панель',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 6,
            'menu_code' =>'mi_000006',
            'languages_id' => 2,
            'languages_code' => 'ru',
            'tittle' => 'СМС сайта',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 7,
            'menu_code' =>'mi_000007',
            'languages_id' => 2,
            'languages_code' => 'ru',
            'tittle' => 'Управление БД',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 8,
            'menu_code' =>'mi_000007',
            'languages_id' => 2,
            'languages_code' => 'ru',
            'tittle' => 'ГитХаб',
            'created_at' => now(),
        ]);
        //
        DB::table($this->tablename)->insert([
            'menu_id' => 1,
            'menu_code' =>'mi_000001',
            'languages_id' => 3,
            'languages_code' =>'en',
            'tittle' => 'Docs',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 2,
            'menu_code' =>'mi_000002',
            'languages_id' => 3,
            'languages_code' => 'en',
            'tittle' => 'Laracasts',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 3,
            'menu_code' =>'mi_000003',
            'languages_id' => 3,
            'languages_code' => 'en',
            'tittle' => 'News',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 4,
            'menu_code' =>'mi_000004',
            'languages_id' => 3,
            'languages_code' => 'en',
            'tittle' => 'Blog',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 5,
            'menu_code' =>'mi_000005',
            'languages_id' => 3,
            'languages_code' => 'en',
            'tittle' => 'Nova',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 6,
            'menu_code' =>'mi_000006',
            'languages_id' => 3,
            'languages_code' => 'en',
            'tittle' => 'Forge',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 7,
            'menu_code' =>'mi_000007',
            'languages_id' => 3,
            'languages_code' => 'en',
            'tittle' => 'Vapor',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 8,
            'menu_code' =>'mi_000007',
            'languages_id' => 3,
            'languages_code' => 'en',
            'tittle' => 'GitHub',
            'created_at' => now(),
        ]);
        //
        DB::table($this->tablename)->insert([
            'menu_id' => 9,
            'menu_code' =>'ml_000001',
            'languages_id' => 1,
            'languages_code' => 'ua',
            'tittle' => 'Український',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 9,
            'menu_code' =>'ml_000001',
            'languages_id' => 1,
            'languages_code' => 'ua',
            'tittle' => 'Російська',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 9,
            'menu_code' =>'ml_000001',
            'languages_id' => 1,
            'languages_code' => 'ua',
            'tittle' => 'Англійська',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 10,
            'menu_code' =>'ml_000002',
            'languages_id' => 2,
            'languages_code' => 'ru',
            'tittle' => 'Украинский',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 10,
            'menu_code' =>'ml_000002',
            'languages_id' => 2,
            'languages_code' => 'ru',
            'tittle' => 'Русский',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 10,
            'menu_code' =>'ml_000002',
            'languages_id' => 2,
            'languages_code' => 'ru',
            'tittle' => 'Английский',
            'created_at' => now(),
        ]);

        DB::table($this->tablename)->insert([
            'menu_id' => 11,
            'menu_code' =>'ml_000003',
            'languages_id' => 3,
            'languages_code' => 'en',
            'tittle' => 'Ukraine',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 11,
            'menu_code' =>'ml_000003',
            'languages_id' => 3,
            'languages_code' => 'en',
            'tittle' => 'Russia',
            'created_at' => now(),
        ]);
        DB::table($this->tablename)->insert([
            'menu_id' => 11,
            'menu_code' =>'ml_000003',
            'languages_id' => 3,
            'languages_code' => 'en',
            'tittle' => 'English',
            'created_at' => now(),
        ]);
    }
}
