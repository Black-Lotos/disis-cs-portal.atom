<?php

use Illuminate\Database\Seeder;

class tbl_messages_group extends Seeder
{
    protected $tablename = 'tbl_messages_group';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table($this->tablename)->insert([
            'messages_group_code'=>"authorization",
            'tittle'=>"Авторизация",
            'created_at'=>now(),
        ]);
        DB::table($this->tablename)->insert([
            'messages_group_code'=>"messages",
            'tittle'=>"Сообщения",
            'created_at'=>now(),
        ]);
        DB::table($this->tablename)->insert([
            'messages_group_code'=>"pagination",
            'tittle'=>"Постраничный вывод",
            'created_at'=>now(),
        ]);
        DB::table($this->tablename)->insert([
            'messages_group_code'=>"password",
            'tittle'=>"Работа с паролем",
            'created_at'=>now(),
        ]);
        DB::table($this->tablename)->insert([
            'messages_group_code'=>"validation",
            'tittle'=>"Проверка вводимых данных",
            'created_at'=>now(),
        ]);
    }
}
