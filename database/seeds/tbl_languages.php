<?php

use Illuminate\Database\Seeder;

class tbl_languages extends Seeder
{
    protected $tablename='tbl_languages';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table($this->tablename)->insert([
            'languages_code' => 'ua',
            'tittle' => 'Українська',
            'created_at'=>now(),
        ]);
        DB::table($this->tablename)->insert([
            'languages_code' => 'ru',
            'tittle' => 'Русский',
            'created_at'=>now(),
        ]);
        DB::table($this->tablename)->insert([
            'languages_code' => 'en',
            'tittle' => 'English',
            'created_at'=>now(),
        ]);
    }
}
