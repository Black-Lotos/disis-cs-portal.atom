<?php

use Illuminate\Database\Seeder;

class tbl_messages_to_language extends Seeder
{
    protected $tablename='tbl_messages_to_language';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table($this->tablename)->insert([
            'messages_code'=>"app_tittle",
            'messages_id'=>1,
            'languages_code'=>"ua",
            'languages_id'=>1,
            'tittle'=>"Сайт підтримки користувачів",
            'created_at'=>now(),
        ]);
        DB::table($this->tablename)->insert([
            'messages_code'=>"app_tittle",
            'messages_id'=>1,
            'languages_code'=>"ru",
            'languages_id'=>2,
            'tittle'=>"Сайт поддержки пользователей",
            'created_at'=>now(),
        ]);
        DB::table($this->tablename)->insert([
            'messages_code'=>"app_tittle",
            'messages_id'=>1,
            'languages_code'=>"en",
            'languages_id'=>3,
            'tittle'=>"User support site",
            'created_at'=>now(),
        ]);
    }
}
