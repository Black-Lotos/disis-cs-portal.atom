<?php

use Illuminate\Database\Seeder;

class tbl_messages extends Seeder
{
    protected $tablename='tbl_messages';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table($this->tablename)->insert([
            'messages_code'=>"app_title",
            'tittle'=>"Сайт поддержки пользователей",
            'messages_group_id'=>"2",
            'messages_group_code'=>"messages",
            'created_at'=>now(),
        ]);
        DB::table($this->tablename)->insert([
            'messages_code'=>"site-nnegc-external",
            'tittle'=>"Сайт ГП «НАЭК «Энергоатом»",
            'messages_group_id'=>"2",
            'messages_group_code'=>"messages",
            'created_at'=>now(),
        ]);
        DB::table($this->tablename)->insert([
            'messages_code'=>"title-vpkis",
            'tittle'=>"Отдел поддержки пользователей ИС",
            'messages_group_id'=>"2",
            'messages_group_code'=>"messages",
            'created_at'=>now(),
        ]);
        DB::table($this->tablename)->insert([
            'messages_code'=>"title-vizis",
            'tittle'=>"Отдел инфраструктурного обеспечения ИС",
            'messages_group_id'=>"2",
            'messages_group_code'=>"messages",
            'created_at'=>now(),
        ]);
    }
}
