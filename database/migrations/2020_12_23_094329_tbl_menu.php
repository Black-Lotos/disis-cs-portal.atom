<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tbl_menu', function (Blueprint $table) {
            $table->integer('menu_id');
            $table->string('menu_code', 50)->unique();
            $table->integer('menu_category_id');
            $table->string('menu_category_code', 50);
            $table->string('mi_url', 250)->nullable();
            $table->string('mi_img', 100)->nullable()->default("/img/empty.png");
            $table->string('mi_access_page', 500)->nullable();
            $table->string('tittle', 128);
            $table->integer('mi_type')->default(0);
            $table->text('description')->nullable();
            $table->smallInteger('mi_auto')->default(0);
            $table->string('mi_class',250)->nullable();
            $table->string('mi_function',250)->nullable();
            $table->string('mi_file',250)->nullable();
            $table->string('mi_arguments',500)->nullable();
            $table->string('mi_callback',500)->nullable();
            $table->string('mi_module',100)->nullable();
            $table->integer('mi_owner');
            $table->integer('weight')->default(0);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->primary(['menu_id', 'menu_category_id', 'menu_code', 'menu_category_code'], 'prmMenu');
        });
        //
        DB::statement("ALTER TABLE tbl_menu CHANGE menu_id menu_id INT(11) NOT NULL AUTO_INCREMENT");
        //
        DB::statement("alter table tbl_menu add constraint FK_menu_category_to_menu foreign key (menu_category_id, menu_category_code)
      references tbl_menu_category (menu_category_id, menu_category_code) on delete restrict on update restrict");
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('tbl_menu');
    }
}
