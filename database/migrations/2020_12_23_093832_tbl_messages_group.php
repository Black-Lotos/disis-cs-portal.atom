<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblMessagesGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tbl_messages_group', function (Blueprint $table) {
            $table->integer('messages_group_id');
            $table->string('messages_group_code',50)->unique();
            $table->string('tittle',128);
            $table->text('description')->nullable();
            $table->integer('weight')->default(0);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->primary(['messages_group_id','messages_group_code'],"prmMessagesGroup");
        });
        DB::statement("ALTER TABLE tbl_messages_group CHANGE messages_group_id messages_group_id INT(11) NOT NULL AUTO_INCREMENT");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('tbl_messages_group');
    }
}
