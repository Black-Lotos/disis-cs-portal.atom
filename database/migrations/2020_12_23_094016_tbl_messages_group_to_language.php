<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblMessagesGroupToLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_messages_group_to_language', function (Blueprint $table) {
            //
            $table->integer('messages_group_lan_id');
            $table->integer('languages_id');
            $table->string('languages_code',50);
            $table->integer('messages_group_id');
            $table->string('messages_group_code',50);
            $table->string('tittle',128);
            $table->text('description')->nullable();
            $table->integer('weight')->default(0);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->primary(['messages_group_lan_id','languages_code','messages_group_code'],'prmMessageGroupToLanguage');
        });
        DB::statement("ALTER TABLE tbl_messages_group_to_language CHANGE messages_group_lan_id message_group_lan_id INT(11) NOT NULL AUTO_INCREMENT");

        DB::statement("alter table tbl_messages_group_to_language add constraint FK_messagegroup_to_language foreign key (languages_id, languages_code)
      references tbl_languages (languages_id, languages_code) on delete restrict on update restrict");

        DB::statement("alter table tbl_messages_group_to_language add constraint FK_messagegroup_to_localization foreign key (messages_group_id, messages_group_code)
      references tbl_messages_group (messages_group_id, messages_group_code) on delete restrict on update restrict");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('tbl_messages_group_to_language');
    }
}
