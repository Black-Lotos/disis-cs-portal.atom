<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tbl_messages', function (Blueprint $table) {
            $table->integer('messages_id');
            $table->string('messages_code',50)->unique();
            $table->integer('messages_group_id');
            $table->string('messages_group_code',50);
            $table->string('tittle',128);
            $table->text('description')->nullable();
            $table->integer('weight')->default(0);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->primary(['messages_id','messages_code'],"prmMessages");
        });
        DB::statement("ALTER TABLE tbl_messages CHANGE messages_id messages_id INT(11) NOT NULL AUTO_INCREMENT");
        DB::statement("alter table tbl_messages add constraint FK_messagesgroup_to_message foreign key (messages_group_id, messages_group_code)
      references tbl_messages_group (messages_group_id, messages_group_code) on delete restrict on update restrict");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('tbl_messages');
    }
}
