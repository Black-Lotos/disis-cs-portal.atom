<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tbl_languages', function (Blueprint $table) {
            $table->integer('languages_id');
            $table->string('languages_code',50)->unique();
            $table->string('tittle',128);
            $table->integer('weight')->default(0);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->primary(['languages_id','languages_code'],"prmLanguages");
        });
        DB::statement("ALTER TABLE tbl_languages CHANGE languages_id languages_id INT(11) NOT NULL AUTO_INCREMENT");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('tbl_languages');
    }
}
