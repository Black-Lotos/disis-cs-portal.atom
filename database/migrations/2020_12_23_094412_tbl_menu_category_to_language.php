<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblMenuCategoryToLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_menu_category_to_language', function (Blueprint $table) {
            $table->integer('menu_category_lan_id');
            $table->integer('menu_category_id');
            $table->string('menu_category_code', 50);
            $table->integer('languages_id');
            $table->string('languages_code', 50);
            $table->string('tittle', 128);
            $table->text('description')->nullable();
            $table->integer('weight')->default(0);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->primary(['menu_category_lan_id','menu_category_id','languages_id','menu_category_code','languages_code'], 'prmMenuCategoryToLanguage');
        });

        DB::statement("ALTER TABLE tbl_menu_category_to_language CHANGE menu_category_lan_id menu_category_lan_id INT(11) NOT NULL AUTO_INCREMENT");

        DB::statement("alter table tbl_menu_category_to_language add constraint FK_menucategory_to_localization foreign key (menu_category_id, menu_category_code)
      references tbl_menu_category (menu_category_id, menu_category_code) on delete restrict on update restrict");


        DB::statement("alter table tbl_menu_category_to_language add constraint FK_menucategory_to_language foreign key (languages_id, languages_code)
      references tbl_languages (languages_id, languages_code) on delete restrict on update restrict");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_menu_category_to_language');
    }
}
