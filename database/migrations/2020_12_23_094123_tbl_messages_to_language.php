<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblMessagesToLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_messages_to_language', function (Blueprint $table) {
            //
            $table->integer('messages_lan_id');
            $table->integer('languages_id');
            $table->string('languages_code',50);
            $table->integer('messages_id');
            $table->string('messages_code',50);
            $table->string('tittle',128);
            $table->text('description')->nullable();
            $table->integer('weight')->default(0);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->primary(['messages_lan_id','languages_code','messages_code'],'prmMessagesToLanguage');
        });
        DB::statement("ALTER TABLE tbl_messages_to_language CHANGE messages_lan_id messages_lan_id INT(11) NOT NULL AUTO_INCREMENT");

        DB::statement("alter table tbl_messages_to_language add constraint FK_messages_to_language foreign key (languages_id, languages_code)
      references tbl_languages (languages_id, languages_code) on delete restrict on update restrict");

        DB::statement("alter table tbl_messages_to_language add constraint FK_messages_to_localization foreign key (messages_id, messages_code)
      references tbl_messages (messages_id, messages_code) on delete restrict on update restrict");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_massages_to_language');
    }
}
