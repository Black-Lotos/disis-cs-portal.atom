<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblMenuToLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_menu_to_language', function (Blueprint $table) {
            //
            $table->integer('menu_lan_id');
            $table->integer('menu_id');
            $table->string('menu_code', 50);
            $table->integer('languages_id');
            $table->string('languages_code',50);
            $table->string('mi_url', 250)->nullable();
            $table->string('mi_img', 100)->nullable()->default("/img/empty.png");
            $table->string('tittle',128);
            $table->text('description')->nullable();
            $table->integer('weight')->default(0);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->primary(['menu_lan_id','menu_id', 'languages_id','menu_code','languages_code'], 'prmMenuToLanguage');
        });

        DB::statement("ALTER TABLE tbl_menu_to_language CHANGE menu_lan_id menu_lan_id INT(11) NOT NULL AUTO_INCREMENT");

        DB::statement("alter table tbl_menu_to_language add constraint FK_menu_to_language foreign key (languages_id, languages_code)
      references tbl_languages (languages_id, languages_code) on delete restrict on update restrict");

        //DB::statement("alter table tbl_menu_to_language add constraint FK_menu_to_localization foreign key (menu_id, menu_code)
        //references tbl_menu (menu_id, menu_code) on delete restrict on update restrict");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_menu_to_language');
    }
}
