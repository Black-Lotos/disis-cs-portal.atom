<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblMenuCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tbl_menu_category', function (Blueprint $table) {
            $table->integer('menu_category_id');
            $table->string('menu_category_code', 50)->unique();
            $table->string('tittle', 128);
            $table->text('description')->nullable();
            $table->integer('weight')->default(0);
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->primary(['menu_category_id', 'menu_category_code'], 'prmMenuCategory');
        });
        DB::statement("ALTER TABLE tbl_menu_category CHANGE menu_category_id menu_category_id INT(11) NOT NULL AUTO_INCREMENT");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('tbl_menu_category');
    }
}
