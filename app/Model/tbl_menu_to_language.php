<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class tbl_menu_to_language extends clBaseModel
{
    protected $table = 'tbl_menu_to_language';
    protected $primaryKey = 'menu_lan_id, menu_id, languages_id, menu_code, languages_code';

    protected $attributes = [
        'menu_id', 'menu_code', 'languages_id', 'languages_code', 'tittle', 'description', 'weight', 'is_active'
    ];

    protected $fillable = [
        'menu_id', 'menu_code', 'languages_id', 'languages_code', 'tittle', 'description', 'weight', 'is_active'
    ];
    protected $hidden = [
        'menu_lan_id'
    ];
    //
    protected $CodeField = 'menu_code';
    //
}
