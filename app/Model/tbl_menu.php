<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class tbl_menu extends clBaseModel
{
    protected $table = 'tbl_menu';

    protected $primaryKey = 'menu_id, menu_category_id, menu_code, menu_category_code';

    protected $attributes = [
        'menu_id', 'menu_code', 'menu_category_id', 'menu_category_code', 'mi_url', 'mi_access_page', 'tittle',
        'mi_type', 'description', 'mi_auto', 'mi_class', 'mi_function', 'mi_file', 'mi_arguments', 'mi_callback',
        'mi_module', 'mi_owner', 'weight', 'is_active'
    ];

    protected $fillable = [
        'menu_code', 'menu_category_id', 'menu_category_code', 'mi_url', 'mi_access_page', 'tittle',
        'mi_type', 'description', 'mi_auto', 'mi_class', 'mi_function', 'mi_file', 'mi_arguments', 'mi_callback',
        'mi_module', 'mi_owner', 'weight', 'is_active'
    ];
    protected $hidden = [
        'menu_id'
    ];

    protected $CodeField = 'menu_code';
    //
    public function GetMenuByCategory($aCategoryCode, $aLanguage) {
        $result =  static::leftjoin('tbl_menu_to_language', "$this->table.$this->CodeField", '=', "tbl_menu_to_language.$this->CodeField")
            //'posts', 'users.id', '=', 'posts.user_id'
            ->where("$this->table.menu_category_code",'=',$aCategoryCode)
            ->where('tbl_menu_to_language.languages_code','=',$aLanguage)
            ->get();
        return $result;
    }
}
