<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Queue\SerializesModels;



class tbl_messages extends Model
{
    //
    protected $table = 'tbl_messages';
    protected $primaryKey = ['messages_lan_id','languages_code','messages_code'];
    protected $attributes = ['languages_id', 'languages_code', 'messages_id', 'messages_code', 'tittle', 'description', 'weight', 'is_active'];

    protected $fillable = [
        'languages_id', 'languages_code', 'messages_id', 'messages_code', 'tittle', 'description', 'weight', 'is_active',
    ];
    public static function IsActive() {
        return static::where('is_active',1)->get();
    }
    public function IsNotActive() {
        return static::where('is_active',0)->get();
    }
    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }

}
