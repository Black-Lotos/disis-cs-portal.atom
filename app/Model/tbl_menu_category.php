<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class tbl_menu_category extends clBaseModel
{
    //
    protected $table = 'tbl_menu_category';
    protected $primaryKey = 'menu_category_id, menu_category_code';

    protected $attributes = [
        'menu_category_id', 'menu_category_code', 'tittle', 'description', 'weight', 'is_active'
    ];

    protected $fillable = [
        'menu_category_code', 'tittle', 'description', 'weight', 'is_active'
    ];
    protected $hidden = [
        'menu_category_id'
    ];
    //
    protected $CodeField = 'menu_category_code';
    //

}
