<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class tbl_messages_to_language extends clBaseModel
{
    //
    protected $table = 'tbl_messages_to_language';
    protected $primaryKey = 'messages_lan_id,languages_code,messages_code';
    protected $attributes = ['languages_id', 'languages_code', 'messages_id', 'messages_code', 'tittle', 'description', 'weight', 'is_active'];

    protected $fillable = [
        'languages_id', 'languages_code', 'messages_id', 'messages_code', 'tittle', 'description', 'weight', 'is_active',
    ];

    //
    protected $CodeField = 'messages_code';
}
