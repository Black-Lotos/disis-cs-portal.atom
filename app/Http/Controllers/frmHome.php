<?php

namespace App\Http\Controllers;
use App;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;


class frmHome extends clBaseController
{
    protected $inGetData = array();

    /**protected function Prepare($aValue=array()) {
        $lvMessages = new \App\Model\tbl_messages_to_language();
        $lvMenu = new \App\Model\tbl_menu();
        $this->inGetData = $aValue;
        $this->inGetData['page_property']['language'] = App::getLocale();
        $this->inGetData['messages']['app_tittle'] = $lvMessages->GetTittle('app_tittle', App::getLocale());
        $this->inGetData['menu'] = $lvMenu->GetMenuByCategory('menu-default',App::getLocale());
        //dd($this->inGetData['menu']);
    }*/
    //
    public function index() {
        parent::StartUp();
        //$language = App::getLocale();
        $this->_inData['page']['content']['middle'] = $this->LoadMiddleContent();
        //dd($this->_inData['page']['content']['middle']);
        return view('pieses/disis-cs-start-up',$this->_inData);
        //return view('authorization.user-reg');
    }
    protected  function  LoadMiddleContent()
    {
        $this->_inData['page']['sub-page-message'] = $this->_Messages->GetTittle('main-info', App::getLocale());
        return view('pieses/disis-cs-home-middle',$this->_inData);
    }
}
