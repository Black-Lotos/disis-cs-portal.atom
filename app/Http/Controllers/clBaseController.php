<?php
namespace App\Http\Controllers;
use App;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class clBaseController extends Controller
{
    //
    protected $_inData = array();
    protected $_inSiteMasterPage = 'site-master-page';
    protected $_inGoUrl = '';
    protected $_Messages;
    protected $_Menu;
    //
    protected function StartUp($aValueMenuCategory='menu-default') {
        $this->_inData = [
            'page' => [
                'menu'=>"",
                'search'=>0,
                'sub-page-message'=>"",
                'property' => [
                    'tittle'=>"",
                    'copyright'=>"",
                    'description'=>"",
                    'keywords'=>"",
                    'language'=>"",
                ],
                'content'=> [
                'left'=>  "",
                'right'=> "",
                'middle'=> "",
                ],
                'user'=> [
                    'status'=>'Користувач',
                ],
            ],
        ];
        $this->_Messages = new \App\Model\tbl_messages_to_language();
        $this->_Menu = new \App\Model\tbl_menu();
        DB::enableQueryLog();
        $this->_inData['page']['property']['language'] = App::getLocale();
        $this->_inData['page']['property']['tittle'] = $this->_Messages->GetTittle('app_tittle', App::getLocale());
        $this->_inData['page']['menu'] = $this->_Menu->GetMenuByCategory($aValueMenuCategory,App::getLocale());
        $this->_inData['page']['menu-language'] = $this->_Menu->GetMenuByCategory('menu-language',App::getLocale());
        //dd($this->_inData['page']['menu-language']);
    }
    protected function  LoadLeftContent() {
        return "LEFT-CONTENT";
    }
    protected function  LoadRightContent() {
        return "RIGHT-CONTENT";
    }
    protected function  LoadMiddleContent() {
        return "CENTER-CONTENT";
    }
}
